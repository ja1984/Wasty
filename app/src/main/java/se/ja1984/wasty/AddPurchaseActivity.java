package se.ja1984.wasty;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;

import org.joda.time.DateTime;

import butterknife.Bind;
import butterknife.ButterKnife;
import se.ja1984.wasty.Models.Purchase;


public class AddPurchaseActivity extends AppCompatActivity {

    private final String TAG = "AddPurchaseActivity";
    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.date)    AppCompatEditText date;
    @Bind(R.id.totalSum)    AppCompatEditText totalSum;
    @Bind(R.id.unnecessarySum)    AppCompatEditText unnecessarySum;
    @Bind(R.id.note)    AppCompatEditText note;

    private DateTime purchaseDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addpurchase);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_discard);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        purchaseDate = DateTime.now();
updateDateText();
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddPurchaseActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        purchaseDate = new DateTime(year, (monthOfYear+1), dayOfMonth,0,0);
                        updateDateText();

                    }
                }, purchaseDate.getYear(), (purchaseDate.getMonthOfYear()-1), purchaseDate.getDayOfMonth()).show();
            }
        });

    }

    private void updateDateText(){
        date.setText(purchaseDate.toString("yyyy-MM-dd"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_addpurchase, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.home){
            finish();
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            Purchase purchase = new Purchase();
            purchase.Note = note.getText().toString();
            purchase.TotalSum = Float.parseFloat(totalSum.getText().toString());
            purchase.UnnecessarySum = Float.parseFloat(unnecessarySum.getText().toString());
            purchase.Date = purchaseDate.toString("yyyy-MM-dd");
            new PurchaseService(this).save(purchase);
            MainActivity.refreshData = true;
            finish();

        }

        return super.onOptionsItemSelected(item);
    }
}
