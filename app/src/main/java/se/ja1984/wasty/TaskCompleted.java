package se.ja1984.wasty;

/**
 * Created by Jack on 2015-07-29.
 */
public interface TaskCompleted<T> {
    void onTaskComplete(T result);
}
