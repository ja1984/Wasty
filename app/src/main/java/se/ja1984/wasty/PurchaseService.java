package se.ja1984.wasty;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;

import se.ja1984.wasty.Models.Purchase;
import se.ja1984.wasty.Models.PurchaseGroup;

/**
 * Created by Jack on 2015-07-29.
 */
public class PurchaseService {
    private Context _context;
    private TaskCompleted _taskCompleted;

    public PurchaseService(Context context){
        _context = context;
    }

    public void loadPurchaseGroups(TaskCompleted taskComplete){
        _taskCompleted = taskComplete;
        new loadPurchaseGroupsTask().execute();
    }

    public void loadPurchaseGroupsForMonth(String month, TaskCompleted taskComplete){
        _taskCompleted = taskComplete;
        new loadPurchaseGroupsByMonthTask(month).execute();
    }

    public void loadPurchases(String month, TaskCompleted taskComplete){
        _taskCompleted = taskComplete;
        new loadPurchaseTask(month).execute();
    }

    public void save(Purchase purchase){
        DatabaseHandler.getInstance(_context).addPurchase(purchase);
    }



    private class loadPurchaseGroupsTask extends AsyncTask<Void, Void, ArrayList<PurchaseGroup>>{

        @Override
        protected ArrayList<PurchaseGroup> doInBackground(Void... params) {
            return DatabaseHandler.getInstance(_context).getPurchaseGroups();
        }

        @Override
        protected void onPostExecute(ArrayList<PurchaseGroup> purchaseGroups) {
            _taskCompleted.onTaskComplete(purchaseGroups);
        }
    }

    private class loadPurchaseGroupsByMonthTask extends AsyncTask<Void, Void, ArrayList<PurchaseGroup>>{

        private String _month;

        public loadPurchaseGroupsByMonthTask(String month){
            _month = month;
        }

        @Override
        protected ArrayList<PurchaseGroup> doInBackground(Void... params) {
            return DatabaseHandler.getInstance(_context).getPurchasesForMonthGroupedByDate(_month);
        }

        @Override
        protected void onPostExecute(ArrayList<PurchaseGroup> purchaseGroups) {
            _taskCompleted.onTaskComplete(purchaseGroups);
        }
    }


    private class loadPurchaseTask extends AsyncTask<Void, Void, ArrayList<Purchase>>{

        private String _month;

        public loadPurchaseTask(String month){
            _month = month;
        }

        @Override
        protected ArrayList<Purchase> doInBackground(Void... params) {
            return DatabaseHandler.getInstance(_context).getPurchases(_month);
        }

        @Override
        protected void onPostExecute(ArrayList<Purchase> purchase) {
            _taskCompleted.onTaskComplete(purchase);
        }
    }

}
