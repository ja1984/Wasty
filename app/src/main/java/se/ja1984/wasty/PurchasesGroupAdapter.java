package se.ja1984.wasty;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.lzyzsd.circleprogress.DonutProgress;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import se.ja1984.wasty.Models.PurchaseGroup;

/**
 * Created by Jack on 2015-07-28.
 */
public class PurchasesGroupAdapter extends RecyclerView.Adapter<PurchasesGroupAdapter.ViewHolder> {

    private ArrayList<PurchaseGroup> purchases;
    private Context _context;

    public PurchasesGroupAdapter(ArrayList<PurchaseGroup> _purchases, Context context){
        this.purchases = _purchases;
        this._context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listitem_purchasegroup, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        final PurchaseGroup purchase = purchases.get(i);
        viewHolder.progress.setProgress(purchase.getProgress());
        viewHolder.title.setText(purchase.getDate().toString("MMMM yyyy"));
        viewHolder.description.setText(purchase.getCostString(_context));
        viewHolder.counter.setText("" + purchase.Purchases);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String month = purchase.getDate().toString("yyyy-MM");
                Intent intent = new Intent(_context, ViewPurchaseGroupActivity.class);
                intent.putExtra("month",month);
                _context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return purchases.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.title) TextView title;
        @Bind(R.id.description) TextView description;
        @Bind(R.id.counter) TextView counter;
        @Bind(R.id.progress) DonutProgress progress;

        public ViewHolder(View view){
            super(view);
            ButterKnife.bind(this,view);

        }
    }
}
