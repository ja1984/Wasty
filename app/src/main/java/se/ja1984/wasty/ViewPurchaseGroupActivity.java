package se.ja1984.wasty;

import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.db.chart.Tools;
import com.db.chart.model.Bar;
import com.db.chart.model.BarSet;
import com.db.chart.view.AxisController;
import com.db.chart.view.ChartView;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import se.ja1984.wasty.Models.Purchase;
import se.ja1984.wasty.Models.PurchaseGroup;

public class ViewPurchaseGroupActivity extends AppCompatActivity {

    private final String TAG = "ViewPurchaseGroupActivity";
    @Bind(R.id.toolbar)     Toolbar toolbar;
    @Bind(R.id.recyclerView)     RecyclerView recyclerView;
    @Bind(R.id.chart)
    BarChart chart;

    private ArrayList<Purchase> purchases;
    private PurchasesAdapter adapter;

    private RecyclerView.LayoutManager layoutManager;

    /** Third chart */
    private boolean mUpdateThree;
    private ArrayList<String> labels;


    private final String[] mLabelsThree= {"00", "04", "08", "12", "16", "20", "24"};
    private final float[][] mValuesThree = {  {4.5f, 5.7f, 4f, 8f, 2.5f, 3f, 6.5f},
            {1.5f, 2.5f, 1.5f, 5f, 5.5f, 5.5f, 3f},
            {8f, 7.5f, 7.8f, 1.5f, 8f, 8f, .5f}};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpurchasegroup);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        purchases = new ArrayList<>();
        adapter = new PurchasesAdapter(purchases,this);
        recyclerView.setAdapter(adapter);



        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String value = extras.getString("month");
            getSupportActionBar().setTitle(String.format(getResources().getString(R.string.title_activity_view_purchasegroup),value));
            loadData(value);
        }
    }



    private void loadData(final String month) {
        new PurchaseService(this).loadPurchases(month, new TaskCompleted<ArrayList<Purchase>>() {
            @Override
            public void onTaskComplete(ArrayList<Purchase> result) {
                purchases.clear();
                purchases.addAll(result);
                adapter.notifyDataSetChanged();

                setupChart(month);
            }
        });
    }

    private void setupChart(String month) {

        new PurchaseService(this).loadPurchaseGroupsForMonth(month, new TaskCompleted<ArrayList<PurchaseGroup>>() {
            @Override
            public void onTaskComplete(ArrayList<PurchaseGroup> result) {

                ArrayList<String> _vals = new ArrayList<String>();
                ArrayList<BarEntry> _tot = new ArrayList<BarEntry>();
                ArrayList<BarEntry> _unn = new ArrayList<BarEntry>();



                labels = new ArrayList<>();
                float[] valuesSum = new float[result.size()];
                float[] valuesUn = new float[result.size()];
                int i = 0;

                for(PurchaseGroup purchase : result){
                    _vals.add(purchase.getDate().toString("dd/M"));

                    _tot.add(new BarEntry(purchase.TotalSum,i));
                    _unn.add(new BarEntry(purchase.UnnecessarySum,i));

                    //valuesSum[i] = purchase.TotalSum;
                    //valuesUn[i] = purchase.UnnecessarySum;
                    i++;
                }




                chart.setDrawValueAboveBar(false);
                chart.setDrawGridBackground(true);
                chart.setAutoScaleMinMaxEnabled(true);
                chart.setMaxVisibleValueCount(10);

                YAxis yLabels = chart.getAxisLeft();
                yLabels.setValueFormatter(new MyValueFormatter());
                chart.getAxisRight().setEnabled(false);

                XAxis xLabels = chart.getXAxis();
                xLabels.setPosition(XAxis.XAxisPosition.TOP);

                chart.setClickable(true);
                chart.setLongClickable(false);
                chart.setBackgroundColor(getResources().getColor(R.color.primary));
                chart.setGridBackgroundColor(getResources().getColor(R.color.primary));

                BarDataSet set1 = new BarDataSet(_tot,"DataSet");
                set1.setColor(getResources().getColor(R.color.white));
                BarDataSet set2 = new BarDataSet(_unn,"DataSet");
                set2.setColor(getResources().getColor(R.color.primary_light));
                //set1.setBarSpacePercent(35f);
                //set2.setBarSpacePercent(35f);

                ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
                dataSets.add(set1);
                dataSets.add(set2);

                BarData data = new BarData(_vals,dataSets);

                data.setValueTextSize(10f);
                chart.setData(data);
                chart.notifyDataSetChanged();

            }
        });
    }

    private ArrayList<Purchase> groupPurchases(ArrayList<Purchase> purchases) {
        ArrayList<Purchase> group = new ArrayList<>();

        Purchase purchase = null;

        for(Purchase p : purchases){
            if(purchase == null){
                purchase = p;
                continue;
            }

            if(p.getDate().isEqual(purchase.getDate())){
                purchase.TotalSum += p.TotalSum;
                purchase.UnnecessarySum += p.UnnecessarySum;
                continue;
            }

            group.add(purchase);
            purchase = null;
        }


        return group;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_viewpurchasegroup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
