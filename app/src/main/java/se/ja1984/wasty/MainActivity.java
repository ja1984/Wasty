package se.ja1984.wasty;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import se.ja1984.wasty.Models.PurchaseGroup;


public class MainActivity extends AppCompatActivity {

    private final String TAG = "MainActivity";
    @Bind(R.id.toolbar)    Toolbar toolbar;
    @Bind(R.id.recyclerView)    RecyclerView recyclerView;
    @Bind(R.id.fab)    FloatingActionButton fab;
    public static boolean refreshData;

    private ArrayList<PurchaseGroup> purchaseGroups;
    private PurchasesGroupAdapter adapter;

    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        purchaseGroups = new ArrayList<>();
        adapter = new PurchasesGroupAdapter(purchaseGroups,this);
        recyclerView.setAdapter(adapter);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AddPurchaseActivity.class));
            }
        });

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        loadData();

    }

    private void loadData() {
        new PurchaseService(this).loadPurchaseGroups(new TaskCompleted<ArrayList<PurchaseGroup>>() {
            @Override
            public void onTaskComplete(ArrayList<PurchaseGroup> result) {
                purchaseGroups.clear();
                purchaseGroups.addAll(result);
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    protected void onResume() {
        if(refreshData){
            Log.d("Re","Refrwh");
            loadData();
            refreshData = false;
        }

        super.onResume();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
