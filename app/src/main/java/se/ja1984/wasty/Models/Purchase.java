package se.ja1984.wasty.Models;

import android.content.Context;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import java.util.ArrayList;

import se.ja1984.wasty.R;
import se.ja1984.wasty.Utils;

/**
 * Created by Jonathan on 2015-07-27.
 */
public class Purchase {
    public long Id;
    public String Date;
    public float TotalSum;
    public float UnnecessarySum;
    public String Note;
    public ArrayList<Article> Articles;

    public Purchase(){}
    public Purchase(float totalSum, float unnecessarySum){
        TotalSum = totalSum;
        UnnecessarySum = unnecessarySum;
    }

    public LocalDateTime getDate(){
        return new DateTime(Date).toLocalDateTime();
    }

    public float getSumDiff(){
        return TotalSum - UnnecessarySum;
    }

    public boolean hasArticles(){
        return Articles != null && Articles.size() > 0;
    }

    public void updateId(long id){
        if(Articles == null || Articles.size() == 0) return;

        for(Article article : Articles){
            article.PurchaseId = id;
        }
    }

    public int getProgress(){
        return (int)((UnnecessarySum / TotalSum) * 100);
    }


    public String getCostString(Context context){
        return String.format(context.getResources().getString(R.string.string_cost),((int)TotalSum + Utils.CurrencySymbol), ((int)UnnecessarySum + Utils.CurrencySymbol));
    }

    public String getNote() {
        return Note == null ? "" : Note;
    }
}
