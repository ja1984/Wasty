package se.ja1984.wasty.Models;

import android.content.Context;
import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

import se.ja1984.wasty.R;
import se.ja1984.wasty.Utils;

/**
 * Created by Jonathan on 2015-07-28.
 */
public class PurchaseGroup {
    public float TotalSum;
    public float UnnecessarySum;
    public int Purchases;
    public String Date;


    public int getProgress(){
        Log.d("getProgress", "" + UnnecessarySum + " - " + TotalSum + " - " + ((UnnecessarySum / TotalSum)*100));
        return (int)((UnnecessarySum / TotalSum) * 100);
    }

    public LocalDateTime getDate(){
        return new DateTime(Date).toLocalDateTime();
    }

    public String getCostString(Context context){
        return String.format(context.getResources().getString(R.string.string_cost), ((int) TotalSum + Utils.CurrencySymbol), ((int) UnnecessarySum + Utils.CurrencySymbol));
    }

}
