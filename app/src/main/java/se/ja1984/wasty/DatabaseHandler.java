package se.ja1984.wasty;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import se.ja1984.wasty.Models.Article;
import se.ja1984.wasty.Models.Purchase;
import se.ja1984.wasty.Models.PurchaseGroup;

/**
 * Created by Jonathan on 2015-07-27.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHandler";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Spendy";
    private static final String ENCODING_SETTING = "PRAGMA encoding = 'UTF-8'";

    private static DatabaseHandler mInstance = null;

    public static DatabaseHandler getInstance(Context ctx){
        if (mInstance == null) {
            mInstance = new DatabaseHandler(ctx);
        }
        return mInstance;
    }


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "onCreate");
        try{
            db.execSQL("CREATE TABLE \"Purchase\" (\n\t" +
                    "`Id`\tINTEGER PRIMARY KEY AUTOINCREMENT,\n\t" +
                    "`Date`\tTEXT,\n\t" +
                    "`TotalSum`\tNUMERIC DEFAULT 0,\n\t" +
                    "`UnnecessarySum`\tNUMERIC DEFAULT 0,\n\t" +
                    "`Note`\tTEXT \n\t" +
                    ");");


            db.execSQL("CREATE TABLE \"Article\" (\n" +
                    "`Id` INTEGER PRIMARY KEY AUTOINCREMENT,\n\t" +
                    "`PurchaseId` INTEGER,\n\t" +
                    "`Name` TEXT,\n\t" +
                    "`Price` NUMERIC DEFAULT 0\n\t" +
                    ");");
        }catch(Exception ex){
            Log.d(TAG,"onCreate - Error");
            ex.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG,"Trying to upgrade database oldVersion: " + oldVersion + " newVersion: " + newVersion);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        if (!db.isReadOnly()) {
            db.execSQL(ENCODING_SETTING);
        }
    }


    public ArrayList<PurchaseGroup> getPurchaseGroups(){
        ArrayList<PurchaseGroup> purchases = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT SUM(TotalSum), SUM(UnnecessarySum), Date, COUNT(Id) FROM Purchase GROUP BY strftime('%Y-%m', Date)", null);
        try{
            while(cursor.moveToNext()){
                PurchaseGroup purchase = new PurchaseGroup();

                purchase.TotalSum = Float.parseFloat(cursor.getString(0));
                purchase.UnnecessarySum = Float.parseFloat(cursor.getString(1));

                purchase.Date = cursor.getString(2);
                purchase.Purchases = Integer.parseInt(cursor.getString(3));

                Log.d("Numer of purchases","" + purchase.Purchases);

                purchases.add(purchase);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            return purchases;
        }finally {
            cursor.close();
        }

        return purchases;

    }

    public ArrayList<PurchaseGroup> getPurchasesForMonthGroupedByDate(String month){
        ArrayList<PurchaseGroup> purchases = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT SUM(TotalSum), SUM(UnnecessarySum), Date FROM Purchase  WHERE strftime('%Y-%m', Date) = '"+month+"' GROUP BY strftime('%Y-%m-%d', Date) ORDER BY Date ASC", null);
        try{
            while(cursor.moveToNext()){
                PurchaseGroup purchase = new PurchaseGroup();

                purchase.TotalSum = Float.parseFloat(cursor.getString(0));
                purchase.UnnecessarySum = Float.parseFloat(cursor.getString(1));
                purchase.Date = cursor.getString(2);
                purchases.add(purchase);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            return purchases;
        }finally {
            cursor.close();
        }

        return purchases;
    }

    public ArrayList<Purchase> getPurchases(){
        ArrayList<Purchase> purchases = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT Id, Date, TotalSum, UnnecessarySum, Note FROM Purchase", null);
        try{
            while(cursor.moveToNext()){
                Purchase purchase = new Purchase();

                purchase.Id = Long.parseLong(cursor.getString(0));
                purchase.Date = cursor.getString(1);
                purchase.TotalSum = Float.parseFloat(cursor.getString(2));
                purchase.UnnecessarySum = Float.parseFloat(cursor.getString(3));
                purchase.Note = cursor.getString(4);

                purchases.add(purchase);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            return purchases;
        }finally {
            cursor.close();
        }

        return purchases;

    }

    public ArrayList<Purchase> getPurchases(String month){
        ArrayList<Purchase> purchases = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        String sql = "SELECT Id, Date, TotalSum, UnnecessarySum, Note FROM Purchase WHERE strftime('%Y-%m', Date) = '" + month + "' ORDER BY Date DESC";
        Log.d("SQL", sql);
        Cursor cursor = db.rawQuery(sql, null);
        try{
            while(cursor.moveToNext()){
                Purchase purchase = new Purchase();

                purchase.Id = Long.parseLong(cursor.getString(0));
                purchase.Date = cursor.getString(1);
                purchase.TotalSum = Float.parseFloat(cursor.getString(2));
                purchase.UnnecessarySum = Float.parseFloat(cursor.getString(3));
                purchase.Note = cursor.getString(4);

                purchases.add(purchase);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            return purchases;
        }finally {
            cursor.close();
        }

        return purchases;

    }


    public void addPurchase(Purchase purchase){
        SQLiteDatabase db = getWritableDatabase();
        try{
            ContentValues values = new ContentValues();

            values.put("Date",purchase.Date);
            values.put("TotalSum",purchase.TotalSum);
            values.put("UnnecessarySum",purchase.UnnecessarySum);
            values.put("Note",purchase.getNote());
            Long purchaseId = db.insert("Purchase",null,values);

            if(purchaseId == -1) return;

            if(purchase.hasArticles()){
                purchase.updateId(purchaseId);
                addArticles(purchase.Articles);
            }


        } catch (Exception e) {
            Log.d(TAG,e.getMessage());
        }
        finally{
        }

    }

    private void addArticles(Article article){
        ArrayList<Article> articles = new ArrayList<>();
        articles.add(article);
        addArticles(articles);
    }

    private void addArticles(ArrayList<Article> articles){
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = null;

            db.beginTransaction();
            for (Article article : articles) {
                values = new ContentValues();
                values.put("PurchaseId", article.PurchaseId);
                values.put("Name", article.Name);
                values.put("Price", article.Price);
                db.insert("Article", null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d(TAG,e.getMessage());
        }
        finally{
            db.endTransaction();
        }

    }

}
