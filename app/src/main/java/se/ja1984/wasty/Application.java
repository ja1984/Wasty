package se.ja1984.wasty;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by Jonathan on 2015-07-27.
 */
public class Application extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
    }
}
