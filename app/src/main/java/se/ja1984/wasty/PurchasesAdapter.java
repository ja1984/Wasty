package se.ja1984.wasty;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import se.ja1984.wasty.Models.Purchase;

/**
 * Created by Jack on 2015-07-28.
 */
public class PurchasesAdapter extends RecyclerView.Adapter<PurchasesAdapter.ViewHolder> {

    private ArrayList<Purchase> purchases;
    private Context _context;

    public PurchasesAdapter(ArrayList<Purchase> _purchases, Context context){
        this.purchases = _purchases;
        this._context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listitem_purchase, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        final Purchase purchase = purchases.get(i);
        viewHolder.title.setText(purchase.getDate().toString("dd MMMM yyyy"));
        viewHolder.description.setText(purchase.getCostString(_context));
    }

    @Override
    public int getItemCount() {
        return purchases.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.title) TextView title;
        @Bind(R.id.description) TextView description;
        @Bind(R.id.counter) TextView counter;

        public ViewHolder(View view){
            super(view);
            ButterKnife.bind(this,view);

        }
    }
}
